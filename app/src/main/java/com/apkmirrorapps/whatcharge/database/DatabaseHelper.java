package com.apkmirrorapps.whatcharge.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.apkmirrorapps.whatcharge.models.CategoryModel;
import com.apkmirrorapps.whatcharge.models.QuoteModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

public class DatabaseHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "WHATSCHARGE";
    private static final String TABLE_NAME_PRESET = "presetmessages";
    private static final String TABLE_NAME_CAT = "category";
    private static final String TABLE_NAME_QUOTE = "quotestable";
    private Context context;
    private String DATABASE_PATH = "";
    private SQLiteDatabase db;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        DATABASE_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        this.context = context;
    }

    public void createDataBase() {
        boolean databaseExist = checkDataBase();
        if(!databaseExist) {
            this.getReadableDatabase();
            this.close();
            try {
                copyDataBase();
            } catch (IOException e) {
                Log.i(TAG, "Error " + e);
            }
        }
    }

    private void copyDataBase() throws IOException {
        try {
            InputStream inputStream = context.getAssets().open(DATABASE_NAME);
            String outFileName = DATABASE_PATH + DATABASE_NAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] mBuffer = new byte[1024];
            int length;
            while ((length = inputStream.read(mBuffer))>0)
            {
                outputStream.write(mBuffer, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.i(TAG, "Error " + e);
        }
    }

    @Override
    public synchronized void close()
    {
        if(db != null)
            db.close();
        super.close();
    }

    private boolean checkDataBase() {
        return new File(DATABASE_PATH + DATABASE_NAME).exists();
    }



    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public boolean addPresetMessages(String content) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("content", content);
        long res = db.insert(TABLE_NAME_PRESET, null, contentValues);
        if (res == -1)
            return false;
        else
            return true;
    }

    public Cursor getAllMessages() {
       SQLiteDatabase db = getWritableDatabase();
       String query = "SELECT * FROM " + TABLE_NAME_PRESET;
       Cursor cursor = db.rawQuery(query, null);
       return cursor;
    }

    public void updateMessage(int id, String message) {
        SQLiteDatabase db = getWritableDatabase();
//        String query = "UPDATE " + TABLE_NAME + " SET content=" + message + " where id = " + id;
//        db.execSQL(query);
        ContentValues contentValues = new ContentValues();
        contentValues.put("content", message);
        db.update(TABLE_NAME_PRESET, contentValues, "id = " + id, null);
        db.close();
    }

    public String getSpecificMessage(int id) {
        String message = null;
        Cursor cursor = getAllMessages();
        while (cursor.moveToNext()) {
            int ids = cursor.getInt(0);
            if (ids == id) {
                message =  cursor.getString(1);
                break;
            }
        }
        return message;
    }

    public boolean deleteItem(int id) {
        Log.d("SomeY", "deleteItem: "+id);
        SQLiteDatabase db = getWritableDatabase();
        try {
            return db.delete(TABLE_NAME_PRESET, "id=" + id, null) > 0;
        } catch (Exception e) {
            Log.d("SomeY", "deleteItem: " + e.getMessage());
            return false;
        }
    }

    public ArrayList<CategoryModel> getAllCategories() {
        ArrayList<CategoryModel> categoryModels = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_CAT, null);
        if (cursor.moveToNext()) {
            do {
                CategoryModel categoryModel = new CategoryModel(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getBlob(2)
                );
                categoryModels.add(categoryModel);
            } while (cursor.moveToNext());
        }
        return categoryModels;
    }

    public int getCountOfQuotes(String cat_id) {
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME_QUOTE + " WHERE cat_id=" + cat_id;
        Cursor cursor = db.rawQuery(query, null);
        return cursor.getCount();
    }

    private String getCatName(int anInt) {
        SQLiteDatabase db = getWritableDatabase();
        String cat_q = "SELECT * from " + TABLE_NAME_CAT + " where cat_id=" + anInt;
        Cursor catCursor = db.rawQuery(cat_q, null);
        String name = "";
        while (catCursor.moveToNext()) {
            name = catCursor.getString(1);
        }
        return name;
    }

    private byte[] getCatIcon(int anInt) {
        SQLiteDatabase db = getWritableDatabase();
        String cat_q = "SELECT * from " + TABLE_NAME_CAT + " where cat_id=" + anInt;
        Cursor catCursor = db.rawQuery(cat_q, null);
        Log.d("QuotesDebugging", "getCatName: " + catCursor.getCount());
        byte[] icon = null;
        while (catCursor.moveToNext()) {
            icon = catCursor.getBlob(2);
        }
        return icon;
    }

    public ArrayList<QuoteModel> getCategoryQuotes(String id) {
        ArrayList<QuoteModel> quoteModels = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME_QUOTE + " WHERE cat_id=" + id;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToNext()) {
            do {
                QuoteModel quoteModel = new QuoteModel(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getInt(3),
                        getCatName(cursor.getInt(3)),
                        getCatIcon(cursor.getInt(3))
                );
                quoteModels.add(quoteModel);
            } while (cursor.moveToNext());
        }
        return quoteModels;
    }

}
