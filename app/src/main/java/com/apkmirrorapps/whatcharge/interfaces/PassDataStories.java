package com.apkmirrorapps.whatcharge.interfaces;

import android.net.Uri;

public interface PassDataStories {
    void passData(Uri uri);
}
