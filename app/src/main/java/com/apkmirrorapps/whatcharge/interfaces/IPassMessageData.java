package com.apkmirrorapps.whatcharge.interfaces;

import com.apkmirrorapps.whatcharge.models.MessageItem;

public interface IPassMessageData {
    void messagePassing(MessageItem messageItem);
}
