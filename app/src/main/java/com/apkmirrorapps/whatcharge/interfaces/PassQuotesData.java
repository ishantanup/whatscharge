package com.apkmirrorapps.whatcharge.interfaces;

public interface PassQuotesData {
    void copyQuotesItem(String quote);
    void shareQuoteData(String quote);
}
