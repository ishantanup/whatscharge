package com.apkmirrorapps.whatcharge.imagecleaner;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apkmirrorapps.whatcharge.R;
import com.apkmirrorapps.whatcharge.adapters.CleanerAdapter;
import com.apkmirrorapps.whatcharge.adapters.StoryImageAdapter;
import com.apkmirrorapps.whatcharge.models.CleanerItem;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ImageCleaner extends AppCompatActivity {

    private TextView textViewImageCleaner;
    private RecyclerView imageCleanerRecycler;

    private static final String TAG = "MainStoryDebug";
    private RecyclerView imagesRecyclerView;
    private File[] files;
    private ArrayList<CleanerItem> paths;

    private CleanerAdapter cleanerAdapter;

    private List<CleanerItem> imageCleanerList;
    private ImageView imageViewDelete;
    private ProgressDialog progressDialog;
    private static int flag = 0;

    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_cleaner);

        adView = (AdView) findViewById(R.id.adview_image_cleaner);
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        adView.loadAd(adRequest);

        paths = new ArrayList<>();
        imageCleanerList = new ArrayList<>();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait..");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_image_cleaner);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        Typeface typeface = Typeface.createFromAsset(getAssets(), "circularstd.ttf");
        initViews();
        setUpFont(typeface);

        try {
            new LoadAllImages().execute();
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }

        imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageCleanerList.size() == 0) {
                    Toast.makeText(ImageCleaner.this, "Please Select more items.", Toast.LENGTH_SHORT).show();
                } else {
                    progressDialog.show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            for (int i = 0; i < imageCleanerList.size(); i++) {
                                Uri uri = imageCleanerList.get(i).getUri();
                                File file = new File(uri.getPath());
                                if (file.delete()) {
                                    flag = 1;
                                } else {
                                    flag = 0;
                                    break;
                                }
                            }
                            try {
                                progressDialog.dismiss();
                                new LoadAllImages().execute();
                            } catch (Exception e) {
                                Toast.makeText(ImageCleaner.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, 1000);
                }
            }
        });

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater menuInflater = getMenuInflater();
//        if (imageCleanerList.size() > 0) {
//            menuInflater.inflate(R.menu.delete_images, menu);
//        }
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.delete_image_menu:
//                Toast.makeText(this, "" + imageCleanerList.size(), Toast.LENGTH_SHORT).show();
//                return true;
//             default:
//                return false;
//        }
//    }

    class LoadAllImages extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                String path = Environment.getExternalStorageDirectory().getPath() + File.separator + "WhatsApp" + File.separator + "Media" + File.separator +
                        "WhatsApp Images";
                File directory = new File(path);
                files = directory.listFiles();
                for (int i = 0; i < files.length; i++)
                {
                    if(files[i].getPath().endsWith("jpg")) {
                        File file = new File(files[i].getPath());
                        Uri uri = Uri.fromFile(file);
                        CleanerItem cleanerItem = new CleanerItem(uri, false);
                        paths.add(cleanerItem);
                    }
                }
            } catch (Exception e) {
                Log.d(TAG, "onCreate: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            cleanerAdapter = new CleanerAdapter(paths, getApplicationContext(), ImageCleaner.this);
            imageCleanerRecycler.setAdapter(cleanerAdapter);
        }
    }

    private void setUpFont(Typeface typeface) {
        textViewImageCleaner.setTypeface(typeface);
        textViewImageCleaner.setTextColor(Color.BLACK);
    }

    private void initViews() {
        imageViewDelete = (ImageView) findViewById(R.id.delete_imagess_imageview);
        textViewImageCleaner = (TextView) findViewById(R.id.textview_image_cleaner_text);
        imageCleanerRecycler = (RecyclerView) findViewById(R.id.image_cleaner_recycler);
        imageCleanerRecycler.setLayoutManager(new GridLayoutManager(this, 3));
        imageCleanerRecycler.setHasFixedSize(true);
    }

    public void prepareSelection(CleanerItem cleanerItem) {
        if (cleanerItem.isChecked()) {
            imageCleanerList.add(cleanerItem);
        } else {
            imageCleanerList.remove(cleanerItem);
        }
    }
}
