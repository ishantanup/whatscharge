package com.apkmirrorapps.whatcharge;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apkmirrorapps.whatcharge.database.DatabaseHelper;
import com.apkmirrorapps.whatcharge.imagecleaner.ImageCleaner;
import com.apkmirrorapps.whatcharge.launcher.AppIntroActivity;
import com.apkmirrorapps.whatcharge.presetmessages.PresetMessages;
import com.apkmirrorapps.whatcharge.scheduler.DirectChat;
import com.apkmirrorapps.whatcharge.scheduler.MessageReminder;
import com.apkmirrorapps.whatcharge.storysaver.StorySaver;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private TextView appName;
    private DrawerLayout drawerLayout;
    private LinearLayout statusSaver;
    private LinearLayout messageReminder;
    private LinearLayout presetMessages;
    private LinearLayout messageSchedule;
    private LinearLayout statusText;
    private LinearLayout deleteImages;

    private LinearLayout shareApplication;
    private LinearLayout rateApplication;

    private DatabaseHelper databaseHelper;
    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = new DatabaseHelper(this);
        databaseHelper.createDataBase();
        openAppIntroIfFirstLaunch();
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        int counter = sharedPreferences.getInt("counter", 0);
        counter += 1;
        editor.putInt("counter", counter);
        editor.commit();

        adView = (AdView) findViewById(R.id.adview_main);
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        adView.loadAd(adRequest);

        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "/WhatsCharge/Stories");

        if (file.mkdir()) {
            Toast.makeText(getApplicationContext(), "Some Folders are created!", Toast.LENGTH_SHORT).show();
        }
        Typeface typeface = Typeface.createFromAsset(getAssets(), "circularstd.ttf");
        initViews();
        setUpFont(typeface);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main_activity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        setUpDrawerLayout(toolbar);

        statusSaver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), StorySaver.class);
                startActivity(intent);
            }
        });
        messageReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MessageReminder.class);
                startActivity(intent);
            }
        });


        presetMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PresetMessages.class);
                startActivity(intent);
            }
        });


        deleteImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ImageCleaner.class);
                startActivity(intent);
            }
        });


        messageSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DirectChat.class);
                startActivity(intent);
            }
        });


        shareApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://play.google.com/store/apps/details?id=" + getPackageName();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "Download complete WhatsApp management tool. The application contains\n\t1. Image Cleaner\n\t2. " +
                        "Message DirectChat\n\t3. Preset Messages\n\t4. Story Downloader\n\nDownload: " + url);
                startActivity(Intent.createChooser(intent, "Select Via"));
            }
        });

        rateApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rateMe();
            }
        });

        statusText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Statuses.class));
            }
        });
    }

    private void rateMe() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent marketIntent = new Intent(Intent.ACTION_VIEW, uri);
        marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(marketIntent);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }

    private void initViews() {
        appName = (TextView) findViewById(R.id.app_name_main_activity);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        statusSaver = (LinearLayout) findViewById(R.id.status_saver);
        statusText = (LinearLayout) findViewById(R.id.status_text);
        messageReminder = (LinearLayout) findViewById(R.id.message_reminder);
        deleteImages = (LinearLayout) findViewById(R.id.delete_images);
        presetMessages = (LinearLayout) findViewById(R.id.preset_message);
        messageSchedule = (LinearLayout) findViewById(R.id.message_scheduler);
        shareApplication = (LinearLayout) findViewById(R.id.share_app);
        rateApplication = (LinearLayout) findViewById(R.id.rate_application);
    }

    private void setUpFont(Typeface typeface) {
        appName.setTypeface(typeface);

        appName.setTextColor(Color.WHITE);
    }

    private void setUpDrawerLayout(Toolbar toolbar) {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_main);
        navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                    toolbar,
                    R.string.open,
                R.string.close
        );
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.rate_drawer:
                Uri uri = Uri.parse("market://details?id=" + getPackageName());
                Intent marketIntent = new Intent(Intent.ACTION_VIEW, uri);
                marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(marketIntent);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                }
                return true;
            case R.id.share_drawer:
                String url = "http://play.google.com/store/apps/details?id=" + getPackageName();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "Download complete WhatsApp management tool. The application contains\n\t1. Image Cleaner\n\t2. " +
                        "Message DirectChat\n\t3. Preset Messages\n\t4. Story Downloader\n\nDownload: " + url);
                startActivity(Intent.createChooser(intent, "Select Via"));
                return true;
            case R.id.credits:
                Intent intent1 = new Intent(getApplicationContext(), Credit.class);
                startActivity(intent1);
                return true;
            default:
                return false;
        }
    }

    private void openAppIntroIfFirstLaunch() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String res = sharedPreferences.getString("ISFIRST", "");
        if (res.equals("")) {
            Intent intent = new Intent(getApplicationContext(), AppIntroActivity.class);
            startActivity(intent);
            finish();
        }

    }


    @Override
    public void onBackPressed() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int counter = sharedPreferences.getInt("counter", 0);
        String did = sharedPreferences.getString("DID", "");
        if (counter != 0 && counter % 5 == 0 && !(did.equals("DID"))) {
            showRatingDialog(sharedPreferences);
        } else {
            super.onBackPressed();
        }
    }

    private void showRatingDialog(SharedPreferences sharedPreferences) {
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Great, You use me!");
        builder.setMessage("As you use me frequently why not rate me on Playstore. It will help me grow");
        builder.setPositiveButton("Rate", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                rateMe();
            }
        });
        builder.setNegativeButton("Next Time", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        builder.setNeutralButton("Already did", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editor.putString("DID", "DID");
                editor.commit();
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
