package com.apkmirrorapps.whatcharge;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

import java.net.URLEncoder;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        Log.d("ErrorReceiver", "onReceive: ");
        String message = intent.getStringExtra("message");
        String number = intent.getStringExtra("number");
        PackageManager packageManager = context.getPackageManager();
        try {
            String url = "https://api.whatsapp.com/send?phone="+ number +"&text=" + URLEncoder.encode(message, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            i.setFlags(FLAG_ACTIVITY_NEW_TASK);
            i.putExtra(Intent.EXTRA_TEXT, message);
            if (i.resolveActivity(packageManager) != null) {
                context.startActivity(i);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
