package com.apkmirrorapps.whatcharge;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.apkmirrorapps.whatcharge.adapters.StatusAdapter;
import com.apkmirrorapps.whatcharge.controllers.CategoryController;
import com.apkmirrorapps.whatcharge.controllers.QuotesController;
import com.apkmirrorapps.whatcharge.database.DatabaseHelper;
import com.apkmirrorapps.whatcharge.interfaces.PassQuotesData;
import com.apkmirrorapps.whatcharge.interfaces.PassQuotesID;
import com.apkmirrorapps.whatcharge.models.QuoteModel;

import java.util.ArrayList;

public class CategoryQuotes extends AppCompatActivity implements PassQuotesData {

    private RecyclerView statusRecycler;
    private DatabaseHelper databaseHelper;
    private ProgressBar progressBar;
    private ArrayList<QuoteModel> quoteModels;
    private static String id = "";
    private StatusAdapter statusAdapter;
    private QuotesController quotesController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_quotes);

        quotesController = new QuotesController(this);

        databaseHelper = new DatabaseHelper(this);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        initViews();

        new LoadAllStatus().execute();
    }

    private void initViews() {
        statusRecycler = (RecyclerView) findViewById(R.id.recycler_quotes);
        statusRecycler.setHasFixedSize(true);
        statusRecycler.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void copyQuotesItem(String quote) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("", quote);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(getApplicationContext(), "Copied to clipboard", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void shareQuoteData(String quote) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, quote);
        startActivity(Intent.createChooser(intent, "Share Via"));
    }

    class LoadAllStatus extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            quoteModels = databaseHelper.getCategoryQuotes(id);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            statusAdapter = new StatusAdapter(quoteModels, getApplicationContext(), quotesController);
            statusRecycler.setAdapter(statusAdapter);

        }
    }

}
