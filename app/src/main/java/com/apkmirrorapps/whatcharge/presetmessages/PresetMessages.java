package com.apkmirrorapps.whatcharge.presetmessages;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.apkmirrorapps.whatcharge.R;
import com.apkmirrorapps.whatcharge.adapters.MessageAdapter;
import com.apkmirrorapps.whatcharge.controllers.PresetController;
import com.apkmirrorapps.whatcharge.database.DatabaseHelper;
import com.apkmirrorapps.whatcharge.interfaces.IPassMessageData;
import com.apkmirrorapps.whatcharge.models.MessageItem;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;

public class PresetMessages extends AppCompatActivity implements IPassMessageData {

    private FloatingActionButton addFab;
    private TextView textDesc;
    private RecyclerView recyclerViewMessage;
    private DatabaseHelper databaseHelper;
    private List<MessageItem> messageItems;
    private MessageAdapter messageAdapter;
    private PresetController presetController;

    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preset_messages);

        adView = (AdView) findViewById(R.id.adview_preset_messages);
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        adView.loadAd(adRequest);


        databaseHelper = new DatabaseHelper(this);
        messageItems = new ArrayList<>();
        presetController = new PresetController(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[] {"android.permission.WRITE_EXTERNAL_STORAGE"}, 0);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_preset_messages);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        Typeface typeface = Typeface.createFromAsset(getAssets(), "circularstd.ttf");
        initViews();

        addFab = (FloatingActionButton) findViewById(R.id.fab_add_preset);
        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AddPresetMessages.class);
                startActivity(intent);
            }
        });

        setTextFonts(typeface);

        Cursor cursor = databaseHelper.getAllMessages();
        int count = 0;
        while (cursor.moveToNext()) {
            MessageItem messageItem = new MessageItem(count++, cursor.getString(1), cursor.getInt(0));
            messageItems.add(messageItem);
        }

        messageAdapter = new MessageAdapter(messageItems, this, presetController);
        recyclerViewMessage.setAdapter(messageAdapter);

    }

    private void setTextFonts(Typeface typeface) {
        textDesc.setTextColor(Color.BLACK);
        textDesc.setTypeface(typeface);
    }

    private void initViews() {
        textDesc = (TextView) findViewById(R.id.textview_preset_messages);

        recyclerViewMessage = (RecyclerView) findViewById(R.id.recyclerPreset);
        recyclerViewMessage.setHasFixedSize(true);
        recyclerViewMessage.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void messagePassing(MessageItem messageItem) {
        Intent intent = new Intent(getApplicationContext(), DetailPresetMessage.class);
        intent.putExtra("message", messageItem.getMessage());
        intent.putExtra("id", messageItem.getId());
        startActivity(intent);
    }
}
