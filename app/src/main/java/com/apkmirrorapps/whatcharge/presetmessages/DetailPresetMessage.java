package com.apkmirrorapps.whatcharge.presetmessages;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.apkmirrorapps.whatcharge.R;
import com.apkmirrorapps.whatcharge.database.DatabaseHelper;

public class DetailPresetMessage extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView editTextN;
    private TextView message;
    private Button sendMessage;
    private Button editMessage;
    private EditText editMessageEditText;
    private DatabaseHelper databaseHelper;
    static int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_preset_message);

        databaseHelper = new DatabaseHelper(this);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "circularstd.ttf");

        Intent intent = getIntent();
        id = intent.getIntExtra("id", 0);
        final String messages = intent.getStringExtra("message");

        initViews();
        setFontFace(typeface);

        message.setText(messages);

        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messM = sendMessage.getText().toString().toLowerCase();
                if (messM.equals("send")) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.setPackage("com.whatsapp");
                        intent.putExtra(Intent.EXTRA_TEXT, "" + message.getText().toString());
                        if (intent.resolveActivity(getPackageManager()) != null) {
                            startActivity(Intent.createChooser(intent, "Share Via"));
                        }
                    } catch (Exception e) {
                        Log.d("WOWOW", "onClick: " + e.getMessage());
                        Toast.makeText(DetailPresetMessage.this, "WhatsApp may not be installed.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    message.setVisibility(View.VISIBLE);
                    editMessageEditText.setVisibility(View.GONE);
                    sendMessage.setText("SEND");
                }
            }
        });

        editMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String editM = editMessage.getText().toString().toLowerCase();
                if (editM.equals("edit")) {
                    message.setVisibility(View.GONE);
                    editMessageEditText.setVisibility(View.VISIBLE);
                    editMessageEditText.setText(message.getText());
                    editMessage.setText("SAVE");
                    sendMessage.setText("CANCEL");
                } else {
                    databaseHelper.updateMessage(id, editMessageEditText.getText().toString());
                    editMessage.setText("EDIT");
                    sendMessage.setText("SEND");
                    message.setVisibility(View.VISIBLE);
                    editMessageEditText.setVisibility(View.GONE);
                    String messagin = databaseHelper.getSpecificMessage(id);
                    message.setText(messagin);
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.delete_images, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.delete_message) {
            if(databaseHelper.deleteItem(id)) {
                Intent intent = new Intent(getApplicationContext(), PresetMessages.class);
                startActivity(intent);
                Toast.makeText(this, "Successfully deleted", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setFontFace(Typeface typeface) {
        editTextN.setTypeface(typeface);
        editTextN.setTextColor(Color.BLACK);
    }

    private void initViews() {
        editMessageEditText = (EditText) findViewById(R.id.edittext_edit_preset_messages);
        editMessageEditText.setVisibility(View.GONE);
        toolbar = (Toolbar) findViewById(R.id.toolbar_edit_preset_messages);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        editTextN = (TextView) findViewById(R.id.textview_edit_preset_messages);
        message = (TextView) findViewById(R.id.mess_edit_preset_messages);
        sendMessage = (Button) findViewById(R.id.send_message);
        editMessage = (Button) findViewById(R.id.edit_message);
    }
}
