package com.apkmirrorapps.whatcharge.presetmessages;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.apkmirrorapps.whatcharge.MainActivity;
import com.apkmirrorapps.whatcharge.R;
import com.apkmirrorapps.whatcharge.database.DatabaseHelper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class AddPresetMessages extends AppCompatActivity {

    private TextView textAddDesc;
    private EditText textEditText;
    private Button addMessage;
    private DatabaseHelper databaseHelper;

    private AdView adView;

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_preset_messages);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial));
        AdRequest adRequestInterstitial = new AdRequest.Builder()
                .build();
        mInterstitialAd.loadAd(adRequestInterstitial);


        adView = (AdView) findViewById(R.id.adview_add_preset_messages);
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        adView.loadAd(adRequest);

        databaseHelper = new DatabaseHelper(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_preset_messages);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        initViews();
        Typeface typeface = Typeface.createFromAsset(getAssets(), "circularstd.ttf");
        setFont(typeface);

        addMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInterstitial();
                String message = textEditText.getText().toString();
                if (message.equals("") || message.length() == 0)  {
                    Toast.makeText(AddPresetMessages.this, "Enter Something", Toast.LENGTH_SHORT).show();
                } else {
                    saveMessageToDatabase(message);
                }
            }
        });

    }

    private void initViews() {
        textAddDesc = (TextView) findViewById(R.id.textview_add_preset_messages);
        textEditText = (EditText) findViewById(R.id.edittext__add_preset_messages);
        addMessage = (Button) findViewById(R.id.save_message_add_preset_messages);
    }

    private void setFont(Typeface typeface) {
        textAddDesc.setTextColor(Color.BLACK);
        textAddDesc.setTypeface(typeface);
    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    private void saveMessageToDatabase(String message) {
        boolean res = databaseHelper.addPresetMessages(message);
        if (res) {
            Toast.makeText(this, "Message Added", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Something went wrong, try again!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }
}
