package com.apkmirrorapps.whatcharge.models;

import android.net.Uri;

public class CleanerItem {
    private Uri uri;
    private boolean isChecked;

    public CleanerItem(Uri uri, boolean isChecked) {
        this.uri = uri;
        this.isChecked = isChecked;
    }

    public Uri getUri() {
        return uri;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
