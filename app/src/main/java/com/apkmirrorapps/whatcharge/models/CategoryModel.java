package com.apkmirrorapps.whatcharge.models;

import android.graphics.drawable.Drawable;

public class CategoryModel {
    private int id;
    private String name;
    private byte[] icon;

    public CategoryModel(int id, String name, byte[] icon) {
        this.id = id;
        this.name = name;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public byte[] getIcon() {
        return icon;
    }
}

