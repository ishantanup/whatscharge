package com.apkmirrorapps.whatcharge.models;

import java.io.Serializable;

public class QuoteModel implements Serializable {
    private int id;
    private String quote;
    private String isFav;
    private int catId;
    private String catName;
    private byte[] catIcon;
    private String color;

    public QuoteModel(int id, String quote, String isFav, int catId, String catName, byte[] catIcon/*, String color*/) {
        this.id = id;
        this.quote = quote;
        this.isFav = isFav;
        this.catId = catId;
        this.catName = catName;
        this.catIcon = catIcon;
        // this.color = color;
    }

    public int getId() {
        return id;
    }

    public String getQuote() {
        return quote;
    }

    public String getIsFav() {
        return isFav;
    }

    public int getCatId() {
        return catId;
    }

    public String getCatName() {
        return catName;
    }

    public byte[] getCatIcon() {
        return catIcon;
    }

    public void setIsFav(String isFav) {
        this.isFav = isFav;
    }

    public String getColor() {
        return color;
    }
}
