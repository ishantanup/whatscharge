package com.apkmirrorapps.whatcharge.models;

public class MessageItem {
    private int number;
    private String message;
    private int id;

    public MessageItem(int number, String message, int id) {
        this.number = number;
        this.message = message;
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public String getMessage() {
        return message;
    }

    public int getId() {
        return id;
    }
}
