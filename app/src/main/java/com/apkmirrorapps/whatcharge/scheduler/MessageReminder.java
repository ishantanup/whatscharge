package com.apkmirrorapps.whatcharge.scheduler;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.apkmirrorapps.whatcharge.AlarmReceiver;
import com.apkmirrorapps.whatcharge.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.hbb20.CountryCodePicker;

import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;

public class MessageReminder extends AppCompatActivity {

    private static final String TAG = "DirectChat Error";
    private TextView textViewS;
    private TextView selectTimeText;
    private CountryCodePicker countryCode;
    private EditText mainNumber;
    private EditText messageMain;
   // private Button sendNow;
    private Button sendLater;
    public ImageView dateSelector;
    public ImageView timeSelector;

    public static Calendar calendar;

    private AdView adView;

    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_reminder);


        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial));
        AdRequest adRequestInterstitial = new AdRequest.Builder()
                .build();
        mInterstitialAd.loadAd(adRequestInterstitial);

        adView = (AdView) findViewById(R.id.adview_scheduler);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        adView.loadAd(adRequest);
        showInterstitial();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_scheduler) ;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        calendar = Calendar.getInstance();

        initViews();

        Typeface typeface = Typeface.createFromAsset(getAssets(), "circularstd.ttf");
        setUpFontFace(typeface);

//        sendNow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String countryCode_string = countryCode.getSelectedCountryCode().toString();
//                String number_string = mainNumber.getText().toString();
//                String message_string = messageMain.getText().toString();
//
//                try {
//                    if (strinOk(countryCode_string, number_string, message_string)) {
//                        String end_number = countryCode_string.replace("+", "") + number_string;
//                        openWhatsApp(end_number, message_string);
//                    } else {
//                        Toast.makeText(DirectChat.this, "Please fill every field.", Toast.LENGTH_SHORT).show();
//                    }
//                } catch (Exception e) {
//                    Log.d(TAG, "onClick: " + e.getMessage());
//                }
//            }
//        });


        sendLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInterstitial();
                if (calendar.getTimeInMillis() > System.currentTimeMillis()) {
                    String countryCode_string = countryCode.getSelectedCountryCode().toString();
                    String number_string = mainNumber.getText().toString();
                    String message_string = messageMain.getText().toString();

                    try {
                        if (strinOk(countryCode_string, number_string, message_string)) {
                            String end_number = countryCode_string.replace("+", "") + number_string;
                            Intent waIntent = new Intent(getApplicationContext(), AlarmReceiver.class);
                            waIntent.putExtra("number", end_number);
                            waIntent.putExtra("message", message_string);
                            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 1, waIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                            } else {
                                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                            }
                            Toast.makeText(MessageReminder.this, "Message Scheduled at " + new Date(calendar.getTimeInMillis()), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MessageReminder.this, "Please fill every field.", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        Log.d(TAG, "onClick: " + e.getMessage());
                    }
                } else {
                    Toast.makeText(MessageReminder.this, "Please select proper time", Toast.LENGTH_SHORT).show();
                }
            }
        });


        dateSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                final int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(MessageReminder.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        timeSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(MessageReminder.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                calendar.set(Calendar.HOUR_OF_DAY, hour);
                                calendar.set(Calendar.MINUTE, minute);
                                calendar.set(Calendar.SECOND, 0);
                            }
                        }, hour, minute, false);
                timePickerDialog.show();

            }
        });
    }


    private void openWhatsApp(String number, String message) {
        PackageManager packageManager = getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);

        try {
            String url = "https://api.whatsapp.com/send?phone="+ number +"&text=" + URLEncoder.encode(message, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            i.putExtra(Intent.EXTRA_TEXT, message);
            if (i.resolveActivity(packageManager) != null) {
                startActivity(i);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean strinOk(String countryCode_string, String number_string, String message_string) {
        if (TextUtils.isEmpty(countryCode_string) || TextUtils.isEmpty(message_string) || TextUtils.isEmpty(number_string)) {
            return false;
        }
        return true;
    }

    private void setUpFontFace(Typeface typeface) {
        textViewS.setTypeface(typeface);
        selectTimeText.setTypeface(typeface);
        textViewS.setTextColor(Color.BLACK);
        selectTimeText.setTextColor(Color.BLACK);

    }

    private void initViews() {
        textViewS = (TextView) findViewById(R.id.textview_scheduler);
        selectTimeText = (TextView) findViewById(R.id.select_time);
        dateSelector = (ImageView) findViewById(R.id.date_selector);
        timeSelector = (ImageView) findViewById(R.id.time_selector);

       // sendNow = (Button) findViewById(R.id.send_now_message);
        sendLater = (Button) findViewById(R.id.send_later_message);
        countryCode = (CountryCodePicker) findViewById(R.id.country_code);
        mainNumber = (EditText) findViewById(R.id.number_main);
        messageMain = (EditText) findViewById(R.id.message_main);

    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

}