package com.apkmirrorapps.whatcharge;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.apkmirrorapps.whatcharge.adapters.CategoryAdapter;
import com.apkmirrorapps.whatcharge.controllers.CategoryController;
import com.apkmirrorapps.whatcharge.database.DatabaseHelper;
import com.apkmirrorapps.whatcharge.interfaces.PassQuotesID;
import com.apkmirrorapps.whatcharge.models.CategoryModel;

import java.util.ArrayList;

public class Statuses extends AppCompatActivity  implements PassQuotesID {

    private RecyclerView catRecycler;
    private DatabaseHelper databaseHelper;
    private ArrayList<CategoryModel> categoryModels;
    private CategoryAdapter categoryAdapter;
    private ProgressBar progressbar;
    private CategoryController categoryController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statuses);

        categoryController = new CategoryController(this);

        progressbar = (ProgressBar) findViewById(R.id.progressbar);

        databaseHelper = new DatabaseHelper(this);
        initRecyclerview();

        new LoadCategories().execute();

    }

    private void initRecyclerview() {
        catRecycler = (RecyclerView) findViewById(R.id.cat_rec);
        catRecycler.setHasFixedSize(true);
        catRecycler.setLayoutManager(new LinearLayoutManager(
                this
        ));
    }

    class LoadCategories extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            categoryModels = databaseHelper.getAllCategories();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressbar.setVisibility(View.GONE);
            categoryAdapter = new CategoryAdapter(categoryModels, getApplicationContext(), databaseHelper, categoryController);
            catRecycler.setAdapter(categoryAdapter);
        }
    }

    @Override
    public void passQuotesIdToCategoryActivity(String id) {
        Intent intent = new Intent(getApplicationContext(), CategoryQuotes.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }


}
