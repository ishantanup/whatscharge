package com.apkmirrorapps.whatcharge.controllers;

import com.apkmirrorapps.whatcharge.interfaces.PassQuotesData;

public class QuotesController {
    PassQuotesData passQuotesData;

    public QuotesController(PassQuotesData passQuotesData) {
        this.passQuotesData = passQuotesData;
    }

    public void shareQuote(String quote) {
        passQuotesData.shareQuoteData(quote);
    }

    public void copyQuote(String quote) {
        passQuotesData.copyQuotesItem(quote);
    }
}
