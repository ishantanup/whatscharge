package com.apkmirrorapps.whatcharge.controllers;

import com.apkmirrorapps.whatcharge.interfaces.PassQuotesID;

public class CategoryController {
    PassQuotesID passQuotesID;

    public CategoryController(PassQuotesID passQuotesID) {
        this.passQuotesID = passQuotesID;
    }

    public void onCatClick(String id) {
        passQuotesID.passQuotesIdToCategoryActivity(id);
    }
}
