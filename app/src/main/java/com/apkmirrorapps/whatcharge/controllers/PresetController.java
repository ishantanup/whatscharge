package com.apkmirrorapps.whatcharge.controllers;

import com.apkmirrorapps.whatcharge.interfaces.IPassMessageData;
import com.apkmirrorapps.whatcharge.models.MessageItem;

public class PresetController {
    IPassMessageData passMessageData;

    public PresetController(IPassMessageData passMessageData) {
        this.passMessageData = passMessageData;
    }

    public void onMessageClick(MessageItem messageItem) {
        passMessageData.messagePassing(messageItem);
    }
}
