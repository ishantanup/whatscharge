package com.apkmirrorapps.whatcharge.controllers;

import android.net.Uri;

import com.apkmirrorapps.whatcharge.interfaces.PassDataStories;

public class StoryController {
    PassDataStories passDataStories;

    public StoryController(PassDataStories passDataStories) {
        this.passDataStories = passDataStories;
    }

    public void OnItemClick(Uri uri) {
        passDataStories.passData(uri);
    }
}
