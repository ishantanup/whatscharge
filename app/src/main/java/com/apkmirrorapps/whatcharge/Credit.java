package com.apkmirrorapps.whatcharge;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class Credit extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView appName;
    private TextView iconsName;
    private TextView iconsCredit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("About & Credit");

        initViews();
        setFontFace();
    }

    private void setFontFace() {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "circularstd.ttf");
        appName.setTypeface(typeface);
        appName.setTextColor(Color.WHITE);

        iconsName.setTypeface(typeface);
        iconsName.setTextColor(Color.BLACK);

        iconsCredit.setText(getString(R.string.description_flaticons));

    }


    private void initViews() {
        appName = (TextView) findViewById(R.id.about_app_text);
        iconsName = (TextView) findViewById(R.id.icons_text);
        iconsCredit = (TextView) findViewById(R.id.icons_credit);
    }
}
