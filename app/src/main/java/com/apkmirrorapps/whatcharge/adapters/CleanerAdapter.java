package com.apkmirrorapps.whatcharge.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.apkmirrorapps.whatcharge.R;
import com.apkmirrorapps.whatcharge.imagecleaner.ImageCleaner;
import com.apkmirrorapps.whatcharge.models.CleanerItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class CleanerAdapter extends RecyclerView.Adapter<CleanerAdapter.ViewHolder> {

    ArrayList<CleanerItem> paths;
    Context context;
    ImageCleaner imageCleaner;

    public CleanerAdapter(ArrayList<CleanerItem> paths, Context context, ImageCleaner imageCleaner) {
        this.paths = paths;
        this.context = context;
        this.imageCleaner = imageCleaner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_cleaner_item, parent,false);
        return new ViewHolder(view, imageCleaner);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CleanerItem cleanerItem = paths.get(position);
        Glide.with(context)
                .load(cleanerItem.getUri())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.placeholder))
                .into(holder.imageView);
        holder.checkBox.setChecked(cleanerItem.isChecked());
        if(cleanerItem.isChecked()) {
            holder.select.setVisibility(View.VISIBLE);
        } else {
            holder.select.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return paths.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CheckBox checkBox;
        ImageView imageView;
        ImageView select;
        ImageCleaner imageCleaner;

        public ViewHolder(View itemView, ImageCleaner imageCleaner) {
            super(itemView);

            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox_image_cleaner);
            imageView = (ImageView) itemView.findViewById(R.id.cleaner_image_item);
            select = (ImageView) itemView.findViewById(R.id.select);
            select.setVisibility(View.INVISIBLE);
            imageView.setOnClickListener(this);
            checkBox.setOnClickListener(this);
            this.imageCleaner = imageCleaner;
        }

        @Override
        public void onClick(View v) {
            try {
                CleanerItem cleanerItem = paths.get(getAdapterPosition());
                cleanerItem.setChecked(!cleanerItem.isChecked());
                notifyDataSetChanged();
                imageCleaner.prepareSelection(cleanerItem);
            } catch (Exception e) {
                Toast.makeText(context, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }
    }
}
