package com.apkmirrorapps.whatcharge.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apkmirrorapps.whatcharge.R;
import com.apkmirrorapps.whatcharge.controllers.CategoryController;
import com.apkmirrorapps.whatcharge.database.DatabaseHelper;
import com.apkmirrorapps.whatcharge.models.CategoryModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder>{

    ArrayList<CategoryModel> categoryModels;
    Context context;
    DatabaseHelper databaseHelper;
    CategoryController categoryController;

    public CategoryAdapter(ArrayList<CategoryModel> categoryModels, Context context, DatabaseHelper databaseHelper, CategoryController categoryController) {
        this.categoryModels = categoryModels;
        this.context = context;
        this.databaseHelper = databaseHelper;
        this.categoryController = categoryController;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cat_main_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CategoryModel categoryModel = categoryModels.get(position);

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "circularstd.ttf");

        holder.catName.setText(categoryModel.getName());
        holder.catName.setTypeface(typeface);
        holder.catName.setTextColor(Color.BLACK);
        holder.quotesCount.setText(databaseHelper.getCountOfQuotes(String.valueOf(categoryModel.getId())) + " " + categoryModel.getName() + " Quotes");

        Glide.with(context).load(categoryModel.getIcon()).into(holder.catIcon);

    }

    @Override
    public int getItemCount() {
        return categoryModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView catName;
        TextView quotesCount;
        ImageView catIcon;
        CardView catContainer;

        public ViewHolder(View itemView) {
            super(itemView);

            catName = (TextView) itemView.findViewById(R.id.cat_name);
            quotesCount = (TextView) itemView.findViewById(R.id.cat_count);
            catIcon = (ImageView) itemView.findViewById(R.id.cat_icon);
            catContainer = (CardView) itemView.findViewById(R.id.cat_container);
            catContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            CategoryModel categoryModel = categoryModels.get(getAdapterPosition());
            categoryController.onCatClick(String.valueOf(categoryModel.getId()));
        }
    }
}
