package com.apkmirrorapps.whatcharge.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apkmirrorapps.whatcharge.R;
import com.apkmirrorapps.whatcharge.controllers.PresetController;
import com.apkmirrorapps.whatcharge.models.MessageItem;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private List<MessageItem> messageItems;
    private Context context;
    private PresetController presetController;

    public MessageAdapter(List<MessageItem> messageItems, Context context, PresetController presetController) {
        this.messageItems = messageItems;
        this.context = context;
        this.presetController = presetController;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.message_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MessageItem messageItem = messageItems.get(position);
        String count = String.valueOf(messageItem.getNumber() + 1);
        holder.numberCount.setText(count);
        holder.message.setText(messageItem.getMessage());
    }

    @Override
    public int getItemCount() {
        return messageItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView numberCount;
        TextView message;
        LinearLayout messageContainer;

        public ViewHolder(View itemView) {
            super(itemView);

            numberCount = (TextView) itemView.findViewById(R.id.number_message_item);
            message = (TextView) itemView.findViewById(R.id.message_message_item);
            messageContainer = (LinearLayout) itemView.findViewById(R.id.message_container);
            messageContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            MessageItem messageItem = messageItems.get(getAdapterPosition());
            presetController.onMessageClick(messageItem);
        }
    }
}
