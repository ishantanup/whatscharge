package com.apkmirrorapps.whatcharge.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apkmirrorapps.whatcharge.R;
import com.apkmirrorapps.whatcharge.controllers.QuotesController;
import com.apkmirrorapps.whatcharge.models.QuoteModel;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Random;

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.ViewHolder> {

    ArrayList<QuoteModel> quoteModels;
    Context context;
    QuotesController quotesController;

    public StatusAdapter(ArrayList<QuoteModel> quoteModels, Context context, QuotesController quotesController) {
        this.quoteModels = quoteModels;
        this.context = context;
        this.quotesController = quotesController;
    }

    String color[] = {
            "#00CDAC",
            "#DA22FF",
            "#E684AE",
            "#79CBCA",
            "#77A1D3",
            "#334d50",
            "#00416A",
            "#2193b0",
            "#2C5364",
            "#f64f59",
            "#c471ed",
            "#493240",
            "#6b6b83"
    };

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        ViewHolder viewHolder;
        switch (viewType) {
            case 0:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adview, parent, false);
                viewHolder = new ViewHolder(view, viewType);
                return viewHolder;
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quote_item, parent, false);
                viewHolder = new ViewHolder(view, viewType);
                return viewHolder;

        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "circularstd.ttf");

        if (quoteModels.get(position) == null) {
            holder.adView.loadAd(new AdRequest.Builder().build());
        } else {
            Random random = new Random();
            try {
                int pos = random.nextInt((color.length - 1) + 1) + 1;
                holder.quoteLayout.setBackgroundColor(Color.parseColor(color[pos]));
            } catch (Exception e) {

            }
            QuoteModel quoteModel = quoteModels.get(position);
            holder.catName.setText(quoteModel.getCatName());
            holder.catName.setTypeface(typeface);
            holder.catName.setTextColor(Color.BLACK);
            holder.quoteTxt.setText(quoteModel.getQuote());
            Glide.with(context).load(quoteModel.getCatIcon()).into(holder.catThumb);
        }
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 1;
        if (quoteModels.get(position) == null) {
            viewType = 0;
        }
        return viewType;
    }

    @Override
    public int getItemCount() {
        return quoteModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView catThumb;
        TextView catName;
        TextView quoteTxt;

        ImageView share;
        ImageView copyToClipboard;

        LinearLayout quoteLayout;

        AdView adView;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);

            if (viewType == 0) {
                adView = (AdView) itemView.findViewById(R.id.adview);
            } else {
                catThumb = (ImageView) itemView.findViewById(R.id.cat_thumb);
                share = (ImageView) itemView.findViewById(R.id.share);
                copyToClipboard = (ImageView) itemView.findViewById(R.id.copy_to_clipbpoard);
                catName = (TextView) itemView.findViewById(R.id.cat_name);
                quoteTxt = (TextView) itemView.findViewById(R.id.quote_txt);
                quoteLayout = (LinearLayout) itemView.findViewById(R.id.quote_layout);

                quoteLayout.setOnClickListener(this);
                copyToClipboard.setOnClickListener(this);
                share.setOnClickListener(this);
            }

        }

        @Override
        public void onClick(View v) {
            QuoteModel quoteModel = quoteModels.get(getAdapterPosition());
            if (v.getId() == R.id.copy_to_clipbpoard) {
                String link = "Download: https://play.google.com/store/apps/details?id=" + context.getPackageName();
                String message = quoteModel.getQuote().toString() + "\n\n"+link;
                quotesController.copyQuote(message);
            } else if (v.getId() == R.id.share) {
                String link = "Download: https://play.google.com/store/apps/details?id=" + context.getPackageName();
                String message = quoteModel.getQuote().toString() + "\n\n"+link;
                quotesController.shareQuote(message);
            }
        }
    }
}
