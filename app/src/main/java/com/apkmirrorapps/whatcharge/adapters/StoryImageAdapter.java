package com.apkmirrorapps.whatcharge.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.apkmirrorapps.whatcharge.R;
import com.apkmirrorapps.whatcharge.controllers.StoryController;

import java.util.ArrayList;


public class StoryImageAdapter extends RecyclerView.Adapter<StoryImageAdapter.ViewHolder> {

    private ArrayList<Uri> paths;
    private Context context;
    private StoryController storyController;

    public StoryImageAdapter(ArrayList<Uri> paths, Context context, StoryController storyController) {
        this.paths = paths;
        this.context = context;
        this.storyController = storyController;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.story_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Uri uri = paths.get(position);
        Glide.with(context).load(uri).into(holder.storyImage);
    }

    @Override
    public int getItemCount() {
        return paths.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView storyImage;

        public ViewHolder(View itemView) {
            super(itemView);

            storyImage = (ImageView) itemView.findViewById(R.id.story_image_item);
            storyImage.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Uri uri = paths.get(getAdapterPosition());
            storyController.OnItemClick(uri);
        }
    }
}
