package com.apkmirrorapps.whatcharge.storysaver.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apkmirrorapps.whatcharge.MainActivity;
import com.apkmirrorapps.whatcharge.R;
import com.apkmirrorapps.whatcharge.adapters.StoryImageAdapter;
import com.apkmirrorapps.whatcharge.controllers.StoryController;
import com.apkmirrorapps.whatcharge.interfaces.PassDataStories;
import com.apkmirrorapps.whatcharge.storysaver.StoryDetailActivity;
import com.apkmirrorapps.whatcharge.storysaver.StoryVideoDetail;

import java.io.File;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends Fragment implements PassDataStories {


    public VideoFragment() {
        // Required empty public constructor
    }


    private static final String TAG = "MainStoryDebug";
    private RecyclerView imagesRecyclerView;
    private File[] files;
    private ArrayList<Uri> paths;
    private StoryImageAdapter storyImageAdapter;
    private StoryController storyController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, container, false);
        try {
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "/WhatsCharge/Stories");

            if (file.mkdir()) {
                Toast.makeText(getContext(), "Created!", Toast.LENGTH_SHORT).show();
            }

            imagesRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_main_videos);
            imagesRecyclerView.setHasFixedSize(true);
            imagesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));

            storyController = new StoryController(this);
            paths = new ArrayList<>();

            new LoadAllVideos().execute();
        } catch (Exception e) {
            Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getContext(), MainActivity.class);
            startActivity(intent);
        }


        return view;
    }

    @Override
    public void passData(Uri uri) {
        Intent intent = new Intent(getContext(), StoryVideoDetail.class);
        String path = uri.getPath();
        intent.putExtra("uri", path);
        startActivity(intent);
    }

    class LoadAllVideos extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                String path = Environment.getExternalStorageDirectory().getPath() + File.separator + "WhatsApp" + File.separator + "Media" + File.separator +
                        ".Statuses";
                File directory = new File(path);
                files = directory.listFiles();
                for (int i = 0; i < files.length; i++)
                {
                    if(files[i].getPath().endsWith("mp4")) {
                        File file = new File(files[i].getPath());
                        Uri uri = Uri.fromFile(file);
                        paths.add(uri);
                    }
                }
            } catch (Exception e) {
                Log.d(TAG, "onCreate: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            storyImageAdapter = new StoryImageAdapter(paths, getContext(), storyController);
            imagesRecyclerView.setAdapter(storyImageAdapter);
        }
    }

}
