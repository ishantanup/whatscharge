package com.apkmirrorapps.whatcharge.storysaver;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.apkmirrorapps.whatcharge.BuildConfig;
import com.bumptech.glide.Glide;
import com.apkmirrorapps.whatcharge.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class StoryDetailActivity extends AppCompatActivity {

    private ImageView anImage;
    private LinearLayout shareImage;
    private LinearLayout downloadImage;
    private LinearLayout setImage;

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_detail);

        initViews();

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial));
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mInterstitialAd.loadAd(adRequest);


        Intent intent = getIntent();
        final String path = intent.getStringExtra("uri");
        File file = new File(path);
        Uri uri = Uri.fromFile(file);

        Glide.with(getApplicationContext()).load(uri).into(anImage);


        shareImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("image/*");
                    Uri uri = FileProvider.getUriForFile(StoryDetailActivity.this, BuildConfig.APPLICATION_ID + ".provider", new File(path));
                    share.putExtra(Intent.EXTRA_STREAM, uri);
                    share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(Intent.createChooser(share,"Share via"));
                } catch (Exception e) {
                    Toast.makeText(StoryDetailActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        downloadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    showInterstitial();
                    BitmapDrawable draw = (BitmapDrawable) anImage.getDrawable();
                    Bitmap bitmap = draw.getBitmap();

                    FileOutputStream outStream = null;
                    File sdCard = Environment.getExternalStorageDirectory();
                    File dir = new File(sdCard.getAbsolutePath() + "/WhatsCharge/Stories");
                    dir.mkdirs();
                    String fileName = String.format("WhatsCharge%d.jpg", System.currentTimeMillis());
                    File outFile = new File(dir, fileName);
                    Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    intent.setData(Uri.fromFile(outFile));
                    sendBroadcast(intent);
                    try {
                        outStream = new FileOutputStream(outFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                        outStream.flush();
                        outStream.close();
                        Toast.makeText(StoryDetailActivity.this, "Saved Successfully!", Toast.LENGTH_SHORT).show();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    Toast.makeText(StoryDetailActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });


        setImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                    whatsappIntent.setType("image/jpg");
                    whatsappIntent.setPackage("com.whatsapp");
                    Uri uri = FileProvider.getUriForFile(StoryDetailActivity.this, BuildConfig.APPLICATION_ID + ".provider", new File(path));
                    whatsappIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    whatsappIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    try {
                        startActivity(whatsappIntent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(StoryDetailActivity.this, "WhatsApp not installed!", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(StoryDetailActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    private void initViews() {
        shareImage = (LinearLayout) findViewById(R.id.share_image_linear);
        anImage = (ImageView) findViewById(R.id.an_image_story_detail);
        downloadImage = (LinearLayout) findViewById(R.id.download_story_linear);
        setImage = (LinearLayout) findViewById(R.id.set_story_linear);

    }
}
