package com.apkmirrorapps.whatcharge.storysaver;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kekstudio.dachshundtablayout.DachshundTabLayout;
import com.kekstudio.dachshundtablayout.indicators.DachshundIndicator;
import com.kekstudio.dachshundtablayout.indicators.LineMoveIndicator;
import com.kekstudio.dachshundtablayout.indicators.PointMoveIndicator;
import com.apkmirrorapps.whatcharge.R;
import com.apkmirrorapps.whatcharge.adapters.SectionPageAdapter;
import com.apkmirrorapps.whatcharge.storysaver.fragments.ImagesFragment;
import com.apkmirrorapps.whatcharge.storysaver.fragments.VideoFragment;

public class StorySaver extends AppCompatActivity {

    private SectionPageAdapter sectionPageAdapter;
    private ViewPager viewPager;
    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_saver);

        adView = (AdView) findViewById(R.id.adview_status);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        adView.loadAd(adRequest);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "circularstd.ttf");
        TextView activityIndicator = (TextView) findViewById(R.id.activity_indicator_story);
        activityIndicator.setText("Story Saver");
        activityIndicator.setTypeface(typeface);
        activityIndicator.setTextColor(Color.BLACK);

        sectionPageAdapter = new SectionPageAdapter(getSupportFragmentManager());

        //setupview Pager
        viewPager = (ViewPager) findViewById(R.id.container);
        setUpViewPager(viewPager);

        //Tablayout
        DachshundTabLayout tabs = (DachshundTabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        PointMoveIndicator pointMoveIndicator = new PointMoveIndicator(tabs);
        pointMoveIndicator.setSelectedTabIndicatorColor(Color.BLACK);
        tabs.setAnimatedIndicator(pointMoveIndicator);

    }

    public void setUpViewPager(ViewPager viewPager) {
        SectionPageAdapter sectionPageAdapter = new SectionPageAdapter(getSupportFragmentManager());
        sectionPageAdapter.addFragment(new ImagesFragment(), "Images");
        sectionPageAdapter.addFragment(new VideoFragment(), "Videos");
        viewPager.setAdapter(sectionPageAdapter);
    }

}
